<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>CIVIC activity form</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">

  <link rel="stylesheet" href="css/styles.css?v=1.0">
  
	<!-- MAP PICKER Dependencies: JQuery and GMaps API should be loaded first -->
	<script src="js/jquery-1.7.2.min.js"></script>
	<!--<script src="js/formScript.js"></script>-->
	<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
	
	<!-- CSS and JS for our code -->
	<link rel="stylesheet" type="text/css" href="css/mappicker.css"/>
	<script src="js/jquery-gmaps-latlon-picker.js"></script>
	<?php
		include('functions.php');
	?>
	

  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>

<body>
  
  <div id="contact-form"> 
  
   <h1>¡Registra tu actividad!</h1>
   <h2>Usa el formulario para incluir tu nueva actividad en el mapa</h2> 
   
   <p id="failure">Oops... Algo anduvo mal.</p> 
   <p id="success">Gracias, tu mensaje ha sido enviado correctamente.</p>
    
   <form method="post" action="processFormData.php">
   	 <h3>Info actividad</h3>
     <!--Activity basic data--> 
      <label for="activity">Nombre de actividad: <span class="required">*</span></label> 
      <input type="text" id="activity" name="activity" value="" placeholder="Huerto urbano" autofocus="autofocus"/>       
         
      <label for="short_description">Descripción de la actividad</label> 
      <textarea rows="4" cols="50" id="long_description" name="long_description" value="" placeholder="Recoger patatas de sol a sol" ></textarea>
      
      <label for="web_activity">Web con info adicional sobre la actividad</label> 
      <input type="url" id="web_activity" name="web_activity" value="" placeholder="www.myActivityWeb.org" />
      
      <!-- 
      <label for="long_description">Descripción larga<span class="required">*</span></label> 
      <textarea id="long_description" name="long_description" placeholder="Escribe toda la información sobre la actividad aquí"></textarea>
      -->
      <!--Activity category data-->
      <!--TEMATICA-->
      <label for="category_topic">Temática de la actividad<span class="required">*</span></label>
       <select id="category_topic" name="category_topic">   
         <option value="cat_topic_social_coin">Moneda social</option>
         <option value="cat_topic_new_economies">Economías</option>
         <option value="cat_topic_corporation_property">Empresa y propiedad</option>
         <option value="cat_topic_eco">Medioambiente, energía y/o sostenibilidad</option>
         <option value="cat_topic_citizenship">Ciudadanía y comunidades</option>
         <option value="cat_topic_education">Educación</option>
         <option value="cat_topic_comunication">Comuncación</option>
         <option value="cat_topic_communitary_spaces">Espacio público, comunitarios, reuso</option>
         <option value="cat_topic_social_justice">Justicia Social</option>
         <option value="cat_topic_procommon">Procomún</option>
         <option value="cat_topic_gender">Género</option>
         <option value="cat_topic_food">Alimentación</option>
         <option value="cat_topic_culture">Cultura y ocio</option>
         <option value="cat_topic_care">Cuidados</option>
         <option value="cat_topic_technology">Tecnología</option>
         <option value="cat_topic_other">Otros</option>
      </select> 
      <label for="extra_category_topic">Si has marcado, otros, indica a qué nueva temática debería pertenecer </label> 
      <input type="text" id="cat_topic_others" name="cat_topic_others" value="" placeholder="Otra categoria" />
      
		<!--TIPO-->
      <label for="category_type">Tipo de actividad<span class="required">*</span></label>
      <select id="category_type" name="category_type">   
         <option value="cat_type_course">Curso</option>
         <option value="cat_type_workshop">Taller</option>
         <option value="cat_type_party">Fiesta</option>
         <option value="cat_type_exposition">Exposición</option>
         <option value="cat_type_concert">Concierto</option>
         <option value="cat_type_meeting">Encuentro</option>
         <option value="cat_type_conference">Conferencia</option>
         <option value="cat_type_cultural_presentation">Presentación Cultural</option>
         <option value="cat_type_assembly">Asamble</option>
         <option value="cat_type_other">Otros</option>
      </select>
      <label for="cat_type_others">Si has marcado, otros, indica a qué nueva tipo debería pertenecer </label> 
      <input type="text" id="cat_type_others" name="cat_type_others" value="" placeholder="Otra categoria" /> 
      
      <!--ASISTENCIA-->
      <label for="category_attendance">Tipo de asistencia<span class="required">*</span></label>
      <select id="category_attendance" name="category_attendance">   
         <option value="category_attendance_free">Libre / Gratuita</option>
         <option value="category_attendance_register">Inscripción previa / Gratuita</option>
         <option value="category_attendance_paid_inplace">De pago en el mismo lugar de la actividad</option>
         <option value="category_attendance_paid_register">De pago en la inscripción previa</option>
      </select>
      <label for="category_attendance_others">Si has marcado, otros, indica a qué nuevo tipo de asistencia debería haber </label> 
      <input type="text" id="category_attendance_others" name="category_attendance_others" value="" placeholder="Otra categoria" />
      
      <!--Activity schedule data-->
       <h3>Horario actividad</h3>
      
      <label for="datetime_start">Cuándo empieza (dd/mm/aaaa --:--) 24 horas:</label> 
      <input type="datetime" id="datetime_start" name="datetime_start"   value="2014-01-01T08:00:00" min="2014-01-01T00:00:00" placeholder="01/01/2014 15:30" autofocus="autofocus"/>
      
      <label for="datetime_end">Cuándo acaba (dd/mm/aaaa --:--) 24 horas: </label> 
      <input type="datetime" id="datetime_end" name="datetime_end" value="2014-01-01T08:00:00" min="2014-01-01T00:00:00" placeholder="01/01/2014 15:30"/>
      
   
      
      <!--Activity geoposition data-->
      <h3>Localiza la actividad</h3>
      <h4>Elige la dirección y pincha el botón "Search".</h4>
      <p>Si no la encuentras siempre puedes hacer zoom y arrastrar el "marcador" o introducir las coordenadas (latitud y longitud) y presionar "Update Map". 
      	<br/><br/>No obstante, siempre deja introducida una dirección para que la gente pregunte al llegar a la zona por si acaso.  </p>
      
      
      <!--MAP PICKER geoposition data-->
      	
     <fieldset class="gllpLatlonPicker">
      	<label for="address">Dirección: <span class="required">*</span></label> 
      	<input type="text" id="address" name="address" class="gllpSearchField"  value="" placeholder="Calle X, 12, 5ºH" autofocus="autofocus" required/>
		
		<input type="button" class="gllpSearchButton" value="search">
		
		<div class="gllpMap">Google Maps</div>
		<br/>
			<label for="map_latitude" class="gllMapValues">Latitud</label>
			<input type="text" id="map_latitude" name="map_latitude" class="gllMapValues gllpLatitude" value="40.416"/>
			
			<label for="map_longitude" class="gllMapValues">Longitud</label>
			<input type="text"  id="map_longitude" name="map_longitude" class="gllMapValues gllpLongitude" value="-3.683"/>
			
			<label for="map_zoom" class="gllMapValues">Zoom</label>
			<input type="text" id="map_zoom" name="map_zoom" class="gllMapValues gllpZoom" value="10"/>
			<input type="button" id="map_updateButton" class="gllMapValues gllpUpdateButton" value="update map">
		<br/>
	</fieldset>
      
      
      <!--Initiative data--> 
      <h3>Información sobre la Iniciativa o la Persona que que organiza la actividad</h3>
      
      <label for="ini_name_registered">Nombre (iniciativa o persona) ya registrado: <span class="required">*</span></label>
 		<select id="ini_name_registered" name="ini_name_registered" required>   
 	 	<?php
 	 		$iniciativasOptions = getListIniciativesForHtmlForm();
			if(count($iniciativasOptions) > 0)
				echo($iniciativasOptions);
     	?>
     	<option value="ini_other">Aun no registrada</option>
     	</select>
      <h4>Si tu iniciativa no está registrada, por favor, indica "otra" en el desplegable anterior y rellena el siguiente formulario. </h4>
      
      <label for="ini_name">Nombre (iniciativa o persona): </label> 
      <input type="text" id="ini_name" name="ini_name" value="" placeholder="Nombre de tu iniciativa, organización, etc"  />
      
      <label for="ini_descri">Descripción: </label> 
      <input type="text" id="ini_descri" name="ini_descri" value="" placeholder="Nombre de tu iniciativa, organización, etc"/>
      
      <label for="ini_mail">Email Address: </label> 
      <input type="email" id="ini_mail" name="ini_mail" value="" placeholder="your@email.com"  /> 
         
      <label for="ini_web">Website: </label> 
      <input type="web" id="ini_web" name="ini_web" value="" placeholder="www.yourWeb.com"/>
      
      <label for="ini_tef">Teléfono: </label> 
      <input type="text" id="ini_tef" name="ini_tef" value="" placeholder="Nombre de tu iniciativa, organización, etc"   /> 
           
      <label for="ini_addres">Dirección: <span class="required">*</span></label> 
      <input type="text" id="ini_addres" name="ini_addres" placeholder="Calle X, 12, 5ºH" />
      	
      <input type="submit" value="Send away!" id="submit" />
   </form> 
</div>
	<script language="javascript">
	// $(document).ready( function() {
		// altert("esto se ha cargado");
   		 // $('#datetime_start').val(new Date().toDateInputValue());
   		 // $('#activity').val("Salta");
	// });
	// function setDateToday(dateID){
		// var date = new Date().toISOString();
		// var dateFormated = date.substring(0, 10);
// 		
// 		
		// var dateField = document.getElementById('date_start').value;
		// alert("Date: " + date + "; \nDateFormated: " + dateFormated + "\nDateField: " + dateField);
        // field = document.querySelector(dateID);
    	// field.value = date;
    	// console.log(field.value);	
	// };
	// window.onload = function(){
		// //alert("hola");
		// document.getElementById('activity').value = "Salta";
		// setDateToday('#date_start');
		// //document.getElementById('date_start').value = new Date().toDateInputValue();
	// };
	
		
	</script>
	
</body>
</html>