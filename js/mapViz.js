// //MY DATA
 var lat_madrid = 40.4167754;
 var long_madrid= -3.7037901999999576;
 var zoom_init  = 12; 


function main(layerURL){
		//alert("Cargando mapa");
		cartodb.createVis('map', layerURL  , {
            shareable: true,
            title: true,
            description: true,
            search: true,
            tiles_loader: true,
            center_lat: lat_madrid,
            center_lon: long_madrid,
            zoom: zoom_init
        })
         .done(function(vis, layers) {
          // layer 0 is the base layer, layer 1 is cartodb layer
          // setInteraction is disabled by default
          layers[1].setInteraction(true);
          layers[1].on('featureOver', function(e, pos, latlng, data) {
            cartodb.log.log(e, pos, latlng, data);
          });

          // you can get the native map to work with it
          // depending if you use google maps or leaflet
          map = vis.getNativeMap();

          // now, perform any operations you need
          // map.setZoom(3)
          // map.setCenter(new google.maps.Latlng(...))
        })
        .error(function(err) {
          console.log(err);
        });
      }
/**********************************
 * GOOGLE MAPS
 **********************************/
//Create Google Map
function gMaps(layerURL){	
	var map;
	var mapOptions={
		zoom: zoom_init,
		center: new google.maps.LatLng(lat_madrid,long_madrid),
		mapTypeID: google.maps.MapTypeId.ROADMAP
		
	};
	map = new google.maps.Map(document.getElementById('map'), mapOptions);
	
	//add the cartodb layer
	cartodb.createLayer(map, layerURL).addTo(map);

}
/**********************************
 * LEAFLET
 **********************************/
function leaflet(layerURL){	
	var map = L.map('map').setView([lat_madrid,long_madrid],zoom_init);
	//base layer
	L.tileLayer('http://a.tile.stamen.com/toner/{z}/{x}/{y}.png', {
	    attribution: 'stamen http://maps.stamen.com/'
	  }).addTo(map);
	  
	  // add the cartodb layer
	  cartodb.createLayer(map, layerUrl).addTo(map);
}
