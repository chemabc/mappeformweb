<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Civic maqueta</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">

  <link rel="stylesheet" href="css/styles.css?v=1.0">
  <link rel="stylesheet" href="http://libs.cartocdn.com/cartodb.js/v3/themes/css/cartodb.css" />
  <?php
  	include ('functions.php'); 
	global $useMappeCartoDBCount;
	$viz_map_url='';
	if($useMappeCartoDBCount)
		$viz_map_url='http://mappemad.cartodb.com/api/v2/viz/8250cbc6-09e7-11e4-8c54-0e230854a1cb/viz.json';
	else {
		$viz_map_url='http://chemabuss.cartodb.com/api/v2/viz/fef1743e-00ee-11e4-865d-0e230854a1cb/viz.json';
	}
  ?>

  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>

<body>
  <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
  <script src="http://libs.cartocdn.com/cartodb.js/v3/cartodb.js"></script>
  <script src="js/mapViz.js"></script>
   
   
  <h1>Civic</h1>
  <h3><a id="form_link" href="./fromActivity.php"> Formulario </a></h3>
  <h3><a id="getData_link" href="./getDataFromDatabase.php"> Iniciativas registradas </a></h3>
  <div id="map"></div>
  <script language="javascript">
  	var mapPhp = "<?php echo $viz_map_url; ?>";
  //	alert(mapPhp);
  	window.onload = main(mapPhp);
  </script>
  
</body>
</html>

<?php

?>